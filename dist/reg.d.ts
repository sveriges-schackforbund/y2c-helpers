export interface Tournament {
    id: number;
    name: string;
    classes: {
        id: number;
        name: string;
        groups: Group[];
    }[];
}
export interface Group {
    id: number;
    name: string;
}
export interface Role {
    orgtype: number;
    orgid: number;
    orgrole: number;
    memberid: number;
    start: string;
    end: string;
    seclevel: number;
    description: string;
}
export interface Member {
    id: number;
    firstname: string;
    lastname: string;
    birthday: string;
    sex: number;
    street: string;
    zipcode: number;
    city: string;
    email?: string;
    phone?: string;
}
export interface Club {
    id: number;
    name: string;
    active: number;
    school?: School;
    street?: string;
    zipcode?: number;
    city?: string;
    noeconomy?: number;
    schoolclub?: number;
    schoolname?: string;
    schoolid?: number;
    countyid?: number;
    vbdescr?: string;
    schoolclubreason?: number;
    phonenr?: string;
    email?: string;
    startdate?: string;
    startseason?: string;
    endseason?: string;
    checkvalues?: {
        year: number;
        referenceid: number;
    }[];
    roles?: Role[];
    registrations?: {
        regstart: string;
        regend: string;
        date: string;
        membershiptype: number;
        fee: number;
        member: Pick<Member, 'id' | 'firstname' | 'lastname' | 'birthday'>;
    }[];
}
export interface School {
    id: number;
    name: string;
    street: string;
    zipcode: number;
    city: string;
    districtid: number;
    countyid: number;
    breakyear?: number;
    springbreak?: number;
    easterbreak?: number;
}
export interface Team {
    id: number;
    name: string;
    clubid: number;
    groupid: number;
    roles: Pick<Role, 'memberid' | 'start'>[];
    players: {
        member: Pick<Member, 'id' | 'sex'>;
    }[];
}
interface Status {
    status: number;
    message: string;
}
export interface RegStatus {
    account?: {};
    accountStatus: Status;
    school?: School;
    schoolStatus: Status;
    clubs?: Club[];
    clubsStatus: Status;
    teams?: Team[];
    players?: number;
    teamsStatus: Status;
    documents?: {
        [key: number]: string;
    };
    documentsStatus: Status;
    verifications?: {};
    verificationsStatus: Status;
}
export interface ApiEmail {
    messageId: string;
    timestamp: number;
    category: string;
    to: string;
    subject: string;
    html: string;
    events: {
        timestamp: number;
        event: string;
    }[];
}
export interface ApiStatus {
    user: number;
    name?: string;
    school?: string;
    districtid?: number;
    nClubs: number;
    nTeams: number;
    tournament: string;
    created: string;
    updated: string;
    comment: string;
    status: {
        school: number;
        clubs: number;
        teams: number;
        documents: number;
        verifications: number;
    };
    emails: ApiEmail[];
    emailSubscribe: 'opted_in' | 'unsubscribed' | 'subscribed';
    removed: boolean;
}
export declare const defaultStatus: {
    accountStatus: {
        status: number;
        message: string;
    };
    schoolStatus: {
        status: number;
        message: string;
    };
    clubsStatus: {
        status: number;
        message: string;
    };
    teamsStatus: {
        status: number;
        message: string;
    };
    documentsStatus: {
        status: number;
        message: string;
    };
    verificationsStatus: {
        status: number;
        message: string;
    };
};
export declare const playingYear: number;
export declare function getSchoolStatus(school: School): {
    schoolStatus: {
        status: number;
        message: string;
    };
    school: School;
};
export declare function getClubsStatus(clubs: Club[], school?: School, teams?: Team[]): {
    clubsStatus: {
        status: number;
        message: string;
    };
    clubs: Club[];
};
export declare function getTeamsStatus(teams: Team[], clubs?: Club[]): {
    teamsStatus: {
        status: number;
        message: string;
    };
    teams: Team[];
};
export declare function getDocumentsStatus(documents?: RegStatus['documents'], clubs?: Club[], teams?: Team[]): {
    documentsStatus: {
        status: number;
        message: string;
    };
    documents: {
        [key: number]: string;
    };
};
export declare function getVerificationsStatus(clubs: Club[], teams: Team[]): {
    verificationsStatus: {
        status: number;
        message: string;
    };
};
export declare const getRegStatus: (status: RegStatus) => RegStatus;
export declare const statusRequest: ({ memdbUrl, y2cApiUrl, token, tournaments }: {
    memdbUrl: string;
    y2cApiUrl: string;
    token: string;
    tournaments: Tournament[];
}) => Promise<Partial<RegStatus>[]>;
export {};
