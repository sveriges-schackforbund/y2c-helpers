"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const isObject = (data) => data && typeof data === 'object';
const serialize = (data) => isObject(data)
    ? Object.keys(data)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(isObject(data[key]) ? JSON.stringify(data[key]) : data[key])}`)
        .join('&')
    : data.toString();
function y2cRequest(_a) {
    var { headers, data } = _a, options = __rest(_a, ["headers", "data"]);
    return axios_1.default.request(Object.assign(Object.assign(Object.assign({}, options), (data && { data: JSON.stringify(data) })), { headers: Object.assign(Object.assign({}, (options.method && options.method.toUpperCase() === 'POST' && { 'Content-Type': 'application/json; charset=UTF-8' })), headers) }));
}
exports.y2cRequest = y2cRequest;
function memdbRequest(_a) {
    var { headers, data } = _a, options = __rest(_a, ["headers", "data"]);
    return axios_1.default.request(Object.assign(Object.assign(Object.assign({ paramsSerializer: serialize }, options), (data && { data: serialize(data) })), { headers: Object.assign(Object.assign({}, (options.method &&
            options.method.toUpperCase() === 'POST' && { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' })), headers) }));
}
exports.memdbRequest = memdbRequest;
//# sourceMappingURL=request.js.map