import { AxiosRequestConfig } from 'axios';
export declare function y2cRequest<R>({ headers, data, ...options }: AxiosRequestConfig): Promise<import("axios").AxiosResponse<R>>;
export declare function memdbRequest<R>({ headers, data, ...options }: AxiosRequestConfig): Promise<import("axios").AxiosResponse<R>>;
