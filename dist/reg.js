"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
exports.defaultStatus = {
    accountStatus: { status: 3, message: '' },
    schoolStatus: { status: 1, message: 'Klicka här för att välja skola' },
    clubsStatus: { status: 0, message: '' },
    teamsStatus: { status: 0, message: '' },
    documentsStatus: { status: 0, message: '' },
    verificationsStatus: { status: 0, message: '' }
};
exports.playingYear = new Date(Date.now() + 1000 * 60 * 60 * 24 * 30 * 6).getFullYear();
function getSchoolStatus(school) {
    const okBreakyear = school.breakyear && school.breakyear >= exports.playingYear;
    if (!okBreakyear) {
        // eslint-disable-next-line no-param-reassign
        school.springbreak = undefined;
        // eslint-disable-next-line no-param-reassign
        school.easterbreak = undefined;
    }
    return {
        schoolStatus: okBreakyear && !!school.springbreak && !!school.easterbreak
            ? {
                status: 3,
                message: ''
            }
            : !okBreakyear || (!school.springbreak && !school.easterbreak)
                ? {
                    status: 1,
                    message: 'Klicka här för att välja lovveckor'
                }
                : !school.springbreak
                    ? {
                        status: 1,
                        message: 'Du behöver välja sportlovsvecka'
                    }
                    : !school.easterbreak
                        ? {
                            status: 1,
                            message: 'Du behöver välja påsklovsvecka'
                        }
                        : exports.defaultStatus.schoolStatus,
        school
    };
}
exports.getSchoolStatus = getSchoolStatus;
function getClubsStatus(clubs, school, teams) {
    const incompleteBoard = clubs.some(club => (club.roles || []).reduce((tot, { orgrole }) => {
        return tot | (orgrole === 100 ? 1 : orgrole === 101 ? 2 : orgrole === 102 ? 4 : 0);
    }, 0) !== 7 && (teams || []).some(({ clubid }) => club.id === clubid));
    return {
        clubsStatus: !clubs.length && school
            ? {
                status: 1,
                message: 'Klicka här för att skapa en klubb'
            }
            : !clubs.length
                ? exports.defaultStatus.clubsStatus
                : clubs.some(club => (!club.registrations || club.registrations.length < 5) && (teams || []).some(({ clubid }) => club.id === clubid))
                    ? {
                        status: 1,
                        message: clubs.length === 1
                            ? 'Klubben har inte tillräckligt många medlemmar'
                            : 'En eller flera klubbar har inte tillräckligt många medlemmar'
                    }
                    : incompleteBoard
                        ? {
                            status: 1,
                            message: clubs.length === 1 ? 'Klubben har ofullständig styrelse' : 'En eller flera klubbar har ofullständig styrelse'
                        }
                        : !teams || !teams.length
                            ? {
                                status: 1,
                                message: 'Du har inga klubbar anmäld till turneringen'
                            }
                            : {
                                status: 3,
                                message: ''
                            },
        clubs
    };
}
exports.getClubsStatus = getClubsStatus;
function getTeamsStatus(teams, clubs = []) {
    return {
        teamsStatus: !clubs.length
            ? exports.defaultStatus.teamsStatus
            : !teams.length
                ? {
                    status: 1,
                    message: 'Klicka här för att göra en anmälan'
                }
                : teams.some(team => team.players.length < 5)
                    ? {
                        status: 1,
                        message: `${teams.length ? 'Alla lag' : 'Laget'} måste bestå av minst fem spelare`
                    }
                    : // eslint-disable-next-line no-bitwise
                        teams.some(team => team.players.reduce((value, player) => value | (player.member.sex + 1), 0) !== 3)
                            ? {
                                status: 1,
                                message: `${teams.length ? 'Alla lag' : 'Laget'} måste bestå av minst en tjej, och en kille`
                            }
                            : teams.some(team => !team.roles.length)
                                ? {
                                    status: 1,
                                    message: `${teams.length ? 'Alla lag' : 'Laget'} måste ha en lagledare`
                                }
                                : {
                                    status: 3,
                                    message: ''
                                },
        teams
    };
}
exports.getTeamsStatus = getTeamsStatus;
function getDocumentsStatus(documents = {}, clubs = [], teams = []) {
    const clubsWithTeam = teams.reduce((obj, { clubid }) => (Object.assign(Object.assign({}, obj), { [clubid]: true })), {});
    return {
        documentsStatus: !clubs.length
            ? exports.defaultStatus.documentsStatus
            : !Object.keys(clubsWithTeam).length
                ? {
                    status: 1,
                    message: `Börja med att anmäla ${clubs.length > 1 ? 'klubbar' : 'klubben'} till turneringen`
                }
                : clubs.every(club => !clubsWithTeam[club.id] ||
                    (+(club.startdate || '').substr(0, 4) < exports.playingYear - 1 ? !!(club.checkvalues || []).length : club.active === 1))
                    ? {
                        status: 3,
                        message: ''
                    }
                    : clubs.every(club => !clubsWithTeam[club.id] || documents[club.id])
                        ? {
                            status: 2,
                            message: 'Vi inväntar era dokument, och hör av oss när de inkommit'
                        }
                        : {
                            status: 1,
                            message: 'Klicka här för att skapa de dokument som behövs'
                        },
        documents
    };
}
exports.getDocumentsStatus = getDocumentsStatus;
function getVerificationsStatus(clubs, teams) {
    const players = teams.reduce((obj, team) => (Object.assign(Object.assign({}, obj), team.players.reduce((obj2, player) => (Object.assign(Object.assign({}, obj2), { [`${team.clubid}-${player.member.id}`]: true })), {}))), {});
    const regs = clubs.flatMap(({ id, registrations }) => (registrations || []).filter(({ member }) => players[`${id}-${member.id}`]));
    return {
        verificationsStatus: !clubs.some(({ registrations }) => registrations && registrations.length)
            ? exports.defaultStatus.verificationsStatus
            : !regs.length
                ? {
                    status: 1,
                    message: 'Ange spelare innan du börjar verifiera medlemmar'
                }
                : regs.every(({ membershiptype, fee }) => membershiptype >= 120 && fee > 0)
                    ? {
                        status: 1,
                        message: 'Klicka här för att skapa medlemsintyganden'
                    }
                    : regs.some(({ membershiptype, fee }) => membershiptype >= 120 && fee > 0)
                        ? {
                            status: 1,
                            message: 'Alla intyganden är inte utskrivna'
                        }
                        : regs.some(({ membershiptype, fee }) => membershiptype >= 120 && fee < 0)
                            ? {
                                status: 2,
                                message: 'Skicka in alla intyganden och vänta på godkännande'
                            }
                            : {
                                status: 3,
                                message: ''
                            }
    };
}
exports.getVerificationsStatus = getVerificationsStatus;
exports.getRegStatus = (status) => {
    const { school, schoolStatus } = status.school ? getSchoolStatus(status.school) : status;
    const { teams, teamsStatus } = status.teams || status.clubs ? getTeamsStatus(status.teams || status.teams || [], status.clubs || status.clubs) : status;
    const { clubs, clubsStatus } = status.clubs || status.school || status.teams ? getClubsStatus(status.clubs || status.clubs || [], school, teams) : status;
    const { documents, documentsStatus } = status.documents || status.clubs || status.teams ? getDocumentsStatus(status.documents, clubs, teams) : status;
    const { verificationsStatus } = status.clubs || status.teams ? getVerificationsStatus(clubs || status.clubs || [], teams || status.teams || []) : status;
    return Object.assign(Object.assign({}, status), { school,
        schoolStatus,
        clubs,
        clubsStatus,
        teams,
        teamsStatus,
        documents,
        documentsStatus,
        verificationsStatus });
};
exports.statusRequest = ({ memdbUrl, y2cApiUrl, token, tournaments }) => (Promise.all([
    _1.memdbRequest({
        url: `${memdbUrl}/api/v2/member/me`,
        headers: {
            Token: token
        },
        params: {
            schema: {
                roles: [
                    {
                        orgtype: 1,
                        orgrole: 1,
                        end: 1,
                        club: {
                            id: 1,
                            name: 1,
                            active: 1,
                            startdate: 1,
                            schoolid: 1,
                            schoolclub: 1,
                            schoolclubreason: 1,
                            comment: 1,
                            changed: 1,
                            school: {
                                id: 1,
                                name: 1,
                                street: 1,
                                zipcode: 1,
                                city: 1,
                                districtid: 1,
                                countyid: 1,
                                comment: 1,
                                breakyear: 1,
                                springbreak: 1,
                                easterbreak: 1
                            },
                            roles: [
                                {
                                    memberid: 1,
                                    end: 1,
                                    orgrole: 1,
                                    filter: '(&(end=1970-01-01)(|(orgrole=150)(orgrole<=102)))'
                                }
                            ],
                            registrations: [
                                {
                                    member: { id: 1, firstname: 1, lastname: 1, birthday: 1 },
                                    regstart: 1,
                                    regend: 1,
                                    date: 1,
                                    fee: 1,
                                    membershiptype: 1,
                                    filter: `(regstart>=${new Date(Date.now() - 1000 * 60 * 60 * 24 * 30 * 8).getFullYear()}-01-01)` // -8 month
                                }
                            ],
                            // referenceid 2 = Fått årsmötesprotokoll
                            checkvalues: [{ year: 1, referenceid: 1, filter: `(&(year=${exports.playingYear - 1})(referenceid=2))` }]
                        },
                        filter: '(&(orgtype=3)(orgrole=150)(club.schoolclub=1))'
                    }
                ]
            }
        }
    }).then(response => {
        if (response && response.data) {
            const { roles } = response.data;
            return {
                school: roles.length ? roles[roles.length - 1].club.school : undefined,
                clubs: roles.map(({ club }) => club)
            };
        }
        return {};
    }),
    Promise.all(tournaments.flatMap(({ classes }) => classes.flatMap(({ groups }) => groups.flatMap(({ id }) => _1.memdbRequest({
        url: `${memdbUrl}/api/v2/tournament/team`,
        headers: {
            Token: token
        },
        params: {
            groupid: id,
            schema: {
                id: 1,
                name: 1,
                clubid: 1,
                roles: [{ memberid: 1, start: 1, end: 1, filter: '(end=1970-01-01)' }],
                players: [{ member: { id: 1, sex: 1 } }]
            }
        }
    }).then(response => (response && response.data ? response.data.map(team => (Object.assign(Object.assign({}, team), { groupid: id }))) : [])))))).then(teamGroups => {
        return {
            teams: teamGroups.flatMap(teams => teams)
        };
    }),
    _1.y2cRequest({
        url: `${y2cApiUrl}/store`,
        headers: {
            Authorization: `Bearer ${token}`
        }
    }).then(response => {
        const ret = {};
        if (response.data.result) {
            ret.documents = response.data.result.prints || {};
            if (response.data.result.school) {
                ret.school = response.data.result.school;
            }
        }
        return ret;
    })
]));
//# sourceMappingURL=reg.js.map